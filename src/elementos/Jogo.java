package elementos;

public interface Jogo {
	public boolean continuar();
	
	public String jogarRodada();
	
	public String obterResultado();
}
