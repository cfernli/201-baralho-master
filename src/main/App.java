package main;

import elementos.Jogo;
import truco.JogoTruco;
import vinteum.JogoVinteUm;

public class App {
	public static void main(String[] args) {
		Jogo jogo = new JogoVinteUm();
		
		while(jogo.continuar()) {
			String saida = jogo.jogarRodada();
			
			System.out.println(saida);
		}
		
		System.out.println(jogo.obterResultado());
	}
}
